# julabo

European Spallation Source ERIC Site-specific EPICS module: julabo

Additional information:

* [Julabo F25HL Documentation](https://confluence.esss.lu.se/display/IS/Julabo+F25+HL)
* [Julabo FL300 Documentation](https://confluence.ess.eu/display/IS/Julabo+FL300)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/julabo-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
