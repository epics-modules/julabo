Release Notes
=============
0.1.20 (9-October-2024)
---
* Added Julabo FL300 model
* Changed module name to be more generic

0.1.19 (21-September-2022)
---
* Added ZNAM and ONAM to Mode-S

0.1.18 (11-February-2022)
---
* Added PV Barcode ID

0.1.17 (1-December-2021)
---
* Updated PVs names
* Fixed name property length
* Updated the structure files
* Aplied database style guidelines
* Removed submodule GUI

0.1.14 (3-November-2021)
---
* Added monitoring device connection PV

0.1.13 (22-March-2021)
---
* Added High/Low Drive/Operating limits for PID PVs

0.1.8 (3-December-2020)
---
* Added @init to all SET protocols avoiding alarms of uninitialized PVs

0.1.7 (2-December-2020)
---
* Fixed Macros on iocsh file

0.1.2 (6-April-2020)
---
* Added Selftunning and Control Dynamics PVs

0.1.1 (6-November-2019)
---
* Updated wait time in protocol file


0.1.0 (26-September-2019)
---
* Initial Release
