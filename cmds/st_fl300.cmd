require julabo

epicsEnvSet("PORTNAME", "JULABOFL300")
epicsEnvSet("IPADDR", "172.30.38.118")
epicsEnvSet("IPPORT", "4001")
epicsEnvSet("P", "SE-TEST:")
epicsEnvSet("R", "JULAFL300:")

iocshLoad("$(julabo_DIR)julabofl300.iocsh", "PORTNAME=$(PORTNAME), IPADDR=$(IPADDR), P=$(P), R=$(R)")

#- Uncomment these to debug communication
#- asynSetTraceIOMask("$(PORTNAME)", 0, 2)
#- asynSetTraceMask("$(PORTNAME)", 0, 9)
