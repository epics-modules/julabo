# This should be a test startup script
require julabo

# Set parameters
epicsEnvSet("IPADDR",       "172.30.244.140") # Replace for the correct MOXA IP
epicsEnvSet("IPPORT",       "4001")
epicsEnvSet("SYS",          "Sys-Sub") # Replace for the correct System-Subsystem
epicsEnvSet("DEV",          "Dis-Dev-Idx") # Replace for the correct Discipline-Device-Index
epicsEnvSet("PORTNAME",     "$(SYS)-$(DEV)")
epicsEnvSet("P",            "$(SYS):")
epicsEnvSet("R",            "$(DEV):")
epicsEnvSet("TEMPSCAN",     "1")
epicsEnvSet("CONFSCAN",     "10")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(julabo_DIR)db")

# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

# Load database defining the EPICS records for Julabo F25-HL
iocshLoad("$(julabo_DIR)julabof25hl.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORT=$(PORTNAME), TEMPSCAN=$(TEMPSCAN), CONFSCAN=$(CONFSCAN)")

iocInit()
